#!/bin/bash
cd
if [ -d blog.git ]; then
	cd blog.git
	OLD_HEAD=$(git rev-parse HEAD)
	git pull origin main
	NEW_HEAD=$(git rev-parse HEAD)
else
	git clone https://codeberg.org/Codeberg/blog.git blog.git
	cd blog.git
	OLD_HEAD=$(git rev-parse HEAD)
	NEW_HEAD=$OLD_HEAD
fi

[ $OLD_HEAD = $NEW_HEAD ] && exit

make OUTPUTDIR=/var/www/blog publish || exit 1
