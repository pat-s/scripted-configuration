#!/usr/bin/env bash

set -ex

MEM_HIGH=73
CPU_MAX=25
LXC_CUSTOM=$(cat <<EOF
# fnetx 2022-01-06
lxc.cgroup2.cpu.weight.nice = 5

lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/btrfs/ci-agent var/lib/docker none defaults,bind,create=dir 0 0
lxc.mount.entry = tmpfs var/lib/docker/volumes tmpfs defaults,nosuid 0 0
security.nesting = true
lxc.cap.drop =
EOF
)
