#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "otto" "sudo"
user_setup "ashimokawa" "sudo"

source "base/base.sh"
setup_sshd

apt-get install -y git

## automatic garbage collection
install_file "admin" "/home/git/garbage-collection.sh"
source "base/systemd.sh"
systemd_timer "backup repos" "*-*-* *:05/15:00" "/root/repo-backup.git/main.sh" "root"
systemd_timer "reresolve wireguard address" "*-*-* *:00/5:00" "/usr/share/doc/wireguard-tools/examples/reresolve-dns/reresolve-dns.sh andreas" "root"
