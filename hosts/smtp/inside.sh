#!/usr/bin/env bash
# SMTP is netcup server, providing emails and some LXC containers
set -ex

source "base/users.sh"

apt-get install -y --no-install-recommends haproxy

source "base/secrets.sh"
install_template "smtp" "/etc/haproxy/haproxy.cfg" || haproxy -c -f /etc/haproxy/haproxy.cfg && service haproxy restart

# sync authorized users file
# not used yet
# install_file "kampenwand" "/var/jump/.ssh/authorized_keys"

# LXC
source "base/base.sh"
apt_install_norecommends lxc apparmor lxc-templates debootstrap
systemctl enable systemd-resolved
line_in_file "/etc/default/lxc-net" 'LXC_DOMAIN="lxc.local"'
ln -nsf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
install_file_v2 "smtp" "/etc/systemd/system/lxc-dns-lxcbr0.service" || systemctl start lxc-dns-lxcbr0.service
