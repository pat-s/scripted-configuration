#!/bin/bash

set -x

cleanup() {
    lxc-stop forgejo || true
    lxc-destroy forgejo || true
}

run() {
    lxc-create --name="forgejo" --template=debian -- --release bullseye
    cat >> /var/lib/lxc/forgejo/config <<'EOF'
security.nesting = true
lxc.cap.drop =
lxc.apparmor.profile = unconfined
EOF
    lxc-start forgejo
    sleep 5
    lxc-attach forgejo -- bash -c 'apt-get install -y git python3-sh python3-furl python3-bs4 python3-requests'
    cp -a . /var/lib/lxc/forgejo/rootfs/scripted-configuration
    lxc-attach forgejo -- bash -c 'cd scripted-configuration && ./hosts/forgejo-ci/tests/inside_test.sh test_all'
}

main() {
    trap "cleanup" EXIT
    cleanup
    run
}

${@:-main}

