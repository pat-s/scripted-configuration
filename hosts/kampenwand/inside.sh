#!/usr/bin/env bash

set -ex

source "base/users.sh"
user_setup "gusted" "sudo"

apt-get install -y --no-install-recommends haproxy 

source "base/secrets.sh"
secret_get "HAPROXY_BASICAUTH_MONITOR"
export HAPROXY_BASICAUTH_MONITOR
secret_get "HAPROXY_BASICAUTH_MIGRATION"
export HAPROXY_BASICAUTH_MIGRATION
secret_get "HAPROXY_BASICAUTH_ADMIN"
export HAPROXY_BASICAUTH_ADMIN
install_file "kampenwand" "/etc/haproxy/ratelimit-whitelist.acl"
install_template "kampenwand" "/etc/haproxy/haproxy.cfg" || haproxy -c -f /etc/haproxy/haproxy.cfg && service haproxy restart

# sync authorized users file
install_file "kampenwand" "/var/jump/.ssh/authorized_keys"

# setup
apt-get install -y --no-install-recommends qemu-system libvirt-clients libvirt-daemon-system virtinst
virsh net-autostart default
virsh net-list --name | grep -xF "default" || virsh net-start default
install_file_v2 "kampenwand" "/etc/systemd/resolved.conf.d/kvm_dns.conf" || systemctl restart systemd-resolved
